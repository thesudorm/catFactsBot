// Libraries
const Discord = require('discord.js');
const fs = require('fs');
const path = require('path');

// Discord client
const client = new Discord.Client();

// Get the bot token
const token = require('./token');

// Global vars
var subscribedUsers = [];
var catFactsArray = [];

var commandsMap = {
    "c!clean": cleanCommand,
    "c!meow": meowCommand
};

// Executed when the program is started and ready
client.on('ready', function(){
    console.log("Cat Facts Logged in! Printing client data...");
    console.log(client.user);

    // read in the file of cat facts
    var filePath = path.join(__dirname, 'facts.txt');

    fs.readFileSync(filePath).toString().split('\n').forEach(function(line){
        catFactsArray.push(line);
    });

    console.log("Facts Loaded!");
});

client.on('message', function(msg){
    
    if(msg.author.bot){ // Ignore messages from bots
        return;
    } 
 
    if(msg.content.substring(0,2) == 'c!'){ // Check to see if message is a command
        //msg.reply("Command!");

        // Check to see if it is a valid command
        if(checkIfValidCommand(msg.content)){
            commandsMap[msg.content](msg);
        } else {
            msg.reply("Invalid command, try again!");
        }
    } else {
        if(!isUserIgnored(msg.author)){ // Check to see if user is ignored
            if(isUserSubscribed(msg.author.id)){
                // If the user is subscribed
                replyWithCatFact(msg);
            } else {
                // Subscribed the user
                subscribeUser(msg.author.id, msg.author.username);
                msg.reply("Thank you for subscribing to CatFactz!");
            }
        }
    }
});

client.login(token.token);

// Helper functions

function isUserIgnored(author){
    if(author.id == 267989532828237826){
        return true
    } else {
        return false;
    }
}

function subscribeUser(id, name){
   subscribedUsers.push({ "id": id, "name": name })
}

// Returns bool if user is in the sunscribed array
// takes the user id
function isUserSubscribed(id){

    // First, check if array is undefined or not
    if (subscribedUsers === undefined || subscribedUsers.length == 0){
        return false;
    }
    
    for (var i = 0; i < subscribedUsers.length; i++){
        if (id == subscribedUsers[i].id){
            return true;
        }
    }

    return false;
}

// Takes a message and will reply to that message with a cat fact
function replyWithCatFact(msg){

    // Generate a random number between 0 and array length
    var randomNum = Math.floor(Math.random() * (catFactsArray.length - 1)) + 0;

    msg.reply(catFactsArray[randomNum]);
}

function checkIfValidCommand(commandString){

    if(commandsMap[commandString] != undefined){
        return true;
    } else {
        return false;
    }
}

// Command functions
function cleanCommand(msg){

    msg.channel.fetchMessages().then(messages => {
        var botMessages = messages.filter(msg => msg.author.bot);
        var commandMessages = messages.filter(msg => msg.content.substring(0,2) == "c!")
        msg.channel.bulkDelete(botMessages);
        msg.channel.bulkDelete(commandMessages);
    }).catch(function(err){
        console.log(err);
    })
}

function meowCommand(msg){
    msg.channel.send("Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow Meow", {
        tts: true
    });
}